
const title = document.getElementsByClassName('tabs-title');

for (let i = 0; i < title.length; i++){
    title[i].addEventListener('click',function (){
        if (!(this.classList.contains('active'))){
            for (let i = 0; i<title.length; i++){
                title[i].classList.remove('active');
            }
            this.classList.add('active');
        }

        const tabsContent = document.querySelectorAll('.tabs-content li');
        const contentId = this.dataset.filter;
        const content = document.getElementById(contentId);
        for (let i = 0; i < tabsContent.length; i++){
            tabsContent[i].classList.remove('active');
        }
        if (content){
            tabsContent[i].classList.add('active');
        }

    })
}